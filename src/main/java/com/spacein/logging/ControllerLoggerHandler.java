package com.spacein.logging;

import java.util.Optional;

import javax.annotation.PostConstruct;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.CommonsRequestLoggingFilter;

import com.spacein.WebUtils;

@Component
public class ControllerLoggerHandler extends CommonsRequestLoggingFilter {

	private static final int MAX_PAYLOAD_LENGTH = 500000;

	@Autowired
	private WebUtils webUtils;

	@PostConstruct
	public void configLogger() {
		this.setIncludeClientInfo(true);
		this.setIncludeQueryString(true);
		this.setIncludePayload(true);
		this.setIncludeHeaders(true);
		this.setMaxPayloadLength(MAX_PAYLOAD_LENGTH);
	}

	@Override
	protected boolean shouldLog(HttpServletRequest request) {
		return getLogger().isInfoEnabled()
				&& webUtils.getAnnotation(request, LogConfig.class).map(LogConfig::isLogRequest).orElse(true);
	}

	@Override
	protected void beforeRequest(HttpServletRequest request, String message) {
		getLogger().info(message);
	}

	@Override
	protected void afterRequest(HttpServletRequest request, String message) {
		getLogger().info(message);
	}

	@Override
	protected String getMessagePayload(HttpServletRequest request) {
		boolean isShowPayload = webUtils.getAnnotation(request, LogConfig.class).map(LogConfig::includePayload)
				.orElse(true);

		Boolean acceptableContentType = Optional.of(request).map(ServletRequest::getContentType)
				.map(type -> !type.contains("multipart") && !type.contains("image")).orElse(true);

		if (isShowPayload && acceptableContentType) {
			return super.getMessagePayload(request);
		} else {
			return "";
		}
	}

	protected Log getLogger() {
		return logger;
	}
}
