package com.spacein.logging;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ ElementType.TYPE, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface LogConfig {

	/**
	 * @return true if request have to be logged
	 */
	boolean isLogRequest() default true;

	/**
	 * @return true if request payload body have to be logged
	 */
	boolean includePayload() default true;

}
