package com.spacein.base.customvalidation;

public interface FieldAlreadyExist {

	boolean isFieldExists(String value, String fieldName);
}
