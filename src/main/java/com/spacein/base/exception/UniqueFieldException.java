package com.spacein.base.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND)
public class UniqueFieldException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9202695265485268418L;

	public UniqueFieldException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UniqueFieldException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public UniqueFieldException(String message) {
		super(message);
	}

}
