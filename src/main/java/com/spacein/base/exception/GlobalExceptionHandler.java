package com.spacein.base.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.spacein.base.dto.ErrorDTO;

@RestControllerAdvice
public class GlobalExceptionHandler {

	@ExceptionHandler(value = { UniqueFieldException.class })
	public ResponseEntity<ErrorDTO> handleUniqueFieldException(RuntimeException ex) {
		System.out.println("error " + ex);
		ErrorDTO dto = new ErrorDTO(ex.getMessage(), ex.getMessage());
		return new ResponseEntity<ErrorDTO>(dto, HttpStatus.UNPROCESSABLE_ENTITY);
	}

	@ExceptionHandler(value = { UserNotValidException.class })
	public ResponseEntity<ErrorDTO> handleUserNOtValidException(RuntimeException ex) {
		System.out.println("error " + ex);
		ErrorDTO dto = new ErrorDTO(ex.getMessage(), ex.getMessage());
		return new ResponseEntity<ErrorDTO>(dto, HttpStatus.UNAUTHORIZED);
	}
	
	@ExceptionHandler(value = { EntityNotFoundException.class })
	public ResponseEntity<ErrorDTO> handleeNTITYnOTfOUNDException(RuntimeException ex) {
		System.out.println("error " + ex);
		ErrorDTO dto = new ErrorDTO(ex.getMessage(), ex.getMessage());
		return new ResponseEntity<ErrorDTO>(dto, HttpStatus.NOT_FOUND);
	}
}
