package com.spacein.base.exception;

public class UserNotValidException extends RuntimeException {

	public UserNotValidException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UserNotValidException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public UserNotValidException(String message) {
		super(message);
	}

}
