package com.spacein.base.customvalidator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import com.spacein.base.customannotation.UniqueField;
import com.spacein.base.customvalidation.FieldAlreadyExist;
import com.spacein.base.util.ApplicationContextProvider;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class UniqueValidator implements ConstraintValidator<UniqueField, Object>{

	@Autowired
    private ApplicationContext applicationContext;
	
	private FieldAlreadyExist service;
	
	private String fieldName;

	@Override
    public void initialize(UniqueField unique) {
        Class<? extends FieldAlreadyExist> clazz = unique.service();
        this.fieldName = unique.fieldName();
        String serviceQualifier = unique.serviceQualifier();
        log.info("executed");
        if (!serviceQualifier.equals("")) {
            this.service = ApplicationContextProvider.getContext().getBean(serviceQualifier, clazz);
        } else {
        	log.info("executed"+ applicationContext + " " + clazz);
        	
            this.service = ApplicationContextProvider.getContext().getBean(clazz);
        }
    }
	
	@Override
	public boolean isValid(Object value, ConstraintValidatorContext context) {
		// TODO Auto-generated method stub
		log.info(value.toString());
		return !service.isFieldExists(value.toString(),fieldName);
	}

}
