/**
 * 
 */
package com.spacein.base.customannotation;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.spacein.base.customvalidation.FieldAlreadyExist;
import com.spacein.base.customvalidator.UniqueValidator;

@Constraint(validatedBy = UniqueValidator.class)
@Documented
@Retention(RUNTIME)
@Target({ FIELD, METHOD })
/**
 * @author Mohan.Gummadi
 *
 */
public @interface UniqueField {

	String message() default "{unique.value.violation}";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

	Class<? extends FieldAlreadyExist> service();

	String serviceQualifier() default "";

	String fieldName();
}
