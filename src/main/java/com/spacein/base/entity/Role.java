package com.spacein.base.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode.Exclude;

@Entity
@Table(name = "je_md_roles")
@Data
public class Role extends BaseEntity {

	@Column(name = "role_cd")
	private String roleCode;

	@Column(name = "role_desc")
	private String roleDesc;

	@Exclude
	@ManyToMany(mappedBy = "roles", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<LoginUser> users;

}
