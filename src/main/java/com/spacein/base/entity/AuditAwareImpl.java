package com.spacein.base.entity;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.AuditorAware;

import com.spacein.base.util.Constants;

public class AuditAwareImpl implements AuditorAware<String> {

	@Autowired
	private HttpServletRequest request;

	@Override
	public Optional<String> getCurrentAuditor() {
		//return Optional.of(request.getHeader("username"));
		return Optional.of("no_user");
	}

}
