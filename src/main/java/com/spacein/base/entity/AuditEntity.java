package com.spacein.base.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Data;

/**
 * @author Mohanarao Gummadi
 *
 *         Created on May 15, 2019
 * 
 */
@Data
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public abstract class AuditEntity extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@CreatedBy
	@Column(name = "createdby", nullable = false, updatable = false)
	private String createdBy;
	@LastModifiedBy
	@Column(name = "updatedby")
	private String updatedBy;
	@CreatedDate
	@Column(name = "createddate", nullable = false, updatable = false)
	private Date createdDate;
	@LastModifiedDate
	@Column(name = "updateddate")
	private Date updatedDate;

}
