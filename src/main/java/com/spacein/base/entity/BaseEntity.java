package com.spacein.base.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;

/**
 * @author Mohanarao Gummadi
 *
 *         Created on May 22, 2019
 * 
 */
@Data
@MappedSuperclass
public abstract class BaseEntity implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 352295862006555237L;
	@Id
	@Column(name = "unid")
	@GenericGenerator(name = "base_id", strategy = "com.spacein.base.util.UUIDGenerator")
	@GeneratedValue(generator = "base_id")
	private String unid;

}
