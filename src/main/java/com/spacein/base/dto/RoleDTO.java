package com.spacein.base.dto;

import lombok.Data;

@Data
public class RoleDTO extends BaseDTO {

	private String roleCode;

	private String roleDesc;

}
