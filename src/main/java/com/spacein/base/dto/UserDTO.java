package com.spacein.base.dto;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class UserDTO extends BaseDTO {

	private String userName;

	private String password;

	@JsonProperty("roles")
	private Set<RoleDTO> roles;

}
