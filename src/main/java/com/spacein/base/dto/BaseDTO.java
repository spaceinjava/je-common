package com.spacein.base.dto;

import java.io.Serializable;

import lombok.Data;

/**
 * @author Mohanarao Gummadi
 *
 * Created on May 22, 2019
 * 
 */
@Data
public abstract class BaseDTO implements Serializable{

	private String unid;
}
