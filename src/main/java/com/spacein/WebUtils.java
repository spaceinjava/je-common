package com.spacein;

import java.lang.annotation.Annotation;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Component
public class WebUtils {

	@Autowired
	private RequestMappingHandlerMapping handlerMapping;

	/**
	 * Get annotation from http servlet request.
	 */
	public <T extends Annotation> Optional<T> getAnnotation(HttpServletRequest request, Class<T> annotation) {
		try {
			return Optional.ofNullable(handlerMapping.getHandler(request))
					.filter(handler -> handler.getHandler() instanceof HandlerMethod)
					.map(handler -> (HandlerMethod) handler.getHandler()).map(HandlerMethod::getMethod)
					.map(method -> method.getAnnotation(annotation));
		} catch (Exception e) {
			log.warn("Bind controller's method can't be resolved for http request", e);
		}
		return Optional.empty();
	}
}
